import { NgModule } from '@angular/core';
import { LoginComponent } from './login.component';
import { LoginRoutingModule } from './login-router.module';
import { CoreModule } from '../core/core.module';

@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    CoreModule,
    LoginRoutingModule,
  ]
})
export class LoginModule { }
