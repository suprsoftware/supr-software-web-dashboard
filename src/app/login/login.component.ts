import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBarRef } from '@angular/material/snack-bar';
import { BehaviorSubject } from 'rxjs';
import { AuthService } from '../core/services/auth/auth.service';
import { DbService } from '../core/services/db/db.service';
import { RouterService } from '../core/services/router/router.service';
import { LoadingSnackBarComponent } from '../core/services/snack-bar/components';
import { SnackBarService } from '../core/services/snack-bar/snack-bar.service';

enum STATE {
  LOGIN = 'LOGIN',
  CREATE = 'CREATE',
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  logInForm: FormGroup;
  state: STATE;
  isLoading$: BehaviorSubject<boolean>;
  isCreatingAccount$: BehaviorSubject<boolean>;
  loadingSnackBarRef!: MatSnackBarRef<LoadingSnackBarComponent>;

  constructor(
    private auth: AuthService,
    private router: RouterService,
    private db: DbService,
    private snackBar: SnackBarService,
  ) {
    this.isLoading$ = this.auth.isLoading$;
    this.isCreatingAccount$ = new BehaviorSubject<boolean>(false);
    this.state = STATE.LOGIN;
    this.logInForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
      retypePassword: new FormControl('')
    });
  }

  ngOnInit(): void {
  }

  get subtitleText(): string {
    switch (this.state) {
      case STATE.LOGIN: {
        return 'Login to get started.';
      }
      case STATE.CREATE: {
        return 'Create an account to get started.';
      }
    }
  }

  get submitButtonText(): string {
    switch (this.state) {
      case STATE.LOGIN: {
        return 'Login';
      }
      case STATE.CREATE: {
        return 'Create account';
      }
    }
  }

  get accountLinkText(): string {
    switch (this.state) {
      case STATE.LOGIN: {
        return `Don't have an account?`;
      }
      case STATE.CREATE: {
        return 'Have an account?';
      }
    }
  }

  get arePasswordFieldsMatching(): boolean {
    const {password, retypePassword} = this.logInForm.value;
    return password === retypePassword;
  }


  async logInOrCreate(): Promise<void> {
    const {email, password} = this.logInForm.value;
    switch (this.state) {
      case STATE.LOGIN: {
        try {
          await this.auth.signIn(email, password);
          await this.router.navigateToDashboard();
        } catch (err) {
          this.signInErrorHandler(err);
        }
        break;
      }
      case STATE.CREATE: {
        try {
          if (!this.arePasswordFieldsMatching) {
            this.snackBar.openSimple('Passwords do not match.');
            break;
          }
          const {user} = await this.auth.createUserWithEmailAndPassword(email, password);
          this.isCreatingAccount$.next(true);
          this.loadingSnackBarRef = this.snackBar.openLoading('Your account is being created.');
          if (!user) { break; }
          const userDocSub = this.db.getUserDocument(user.uid).snapshotChanges()
            .subscribe({
              next: snapshot => {
                if (snapshot.payload.exists) {
                  userDocSub.unsubscribe();
                  this.loadingSnackBarRef?.dismiss();
                  this.router.navigateToDashboard()
                    .then(() => { this.isCreatingAccount$.next(false); });
                }
              },
              error: () => { console.log('Error fetching user doc snapshot.'); }
            });
        } catch (err) {
          console.error(err);
          this.loadingSnackBarRef?.dismiss();
          this.snackBar.openSimple('Account creation failed.');
        }
        break;
      }
    }
  }

  signInErrorHandler(err: any): void {
    this.isLoading$.next(false);
    switch (err.code) {
      case 'auth/invalid-email': {
        this.snackBar.openSimple('Invalid email.');
        break;
      }
      case 'auth/user-disabled': {
        this.snackBar.openSimple('User account is disabled.');
        break;
      }
      case 'auth/user-not-found': {
        this.snackBar.openSimple('User does not exist.');
        break;
      }
      case 'auth/wrong-password': {
        this.snackBar.openSimple('Invalid password.');
        break;
      }
      default: {
        this.snackBar.openSimple('Unexpected sign in error.');
        break;
      }
    }
  }

  switchState(): void {
    this.logInForm.controls.retypePassword.reset('');
    this.state = this.state === STATE.LOGIN ? STATE.CREATE : STATE.LOGIN;
  }

  sendPasswordResetLink(): void {
    const {email} = this.logInForm.value;
    if (!email) {
      this.snackBar.openSimple('Please enter your email below.');
    } else {
      this.auth.sendPasswordResetEmail(email);
    }
  }

}
