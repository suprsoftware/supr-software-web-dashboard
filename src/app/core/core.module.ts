import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { AuthService } from './services/auth/auth.service';
import { DbService } from './services/db/db.service';
import { RouterService } from './services/router/router.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SnackBarService } from './services/snack-bar/snack-bar.service';
import { LoadingSnackBarComponent, SimpleSnackBarComponent } from './services/snack-bar/components';
import { TokenService } from './services/token/token.service';

@NgModule({
  declarations: [LoadingSnackBarComponent, SimpleSnackBarComponent],
  imports: [
    BrowserAnimationsModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
  ],
  exports: [
    BrowserAnimationsModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
  ],
  providers: [
    AuthService,
    DbService,
    RouterService,
    SnackBarService,
    TokenService,
  ]
})
export class CoreModule { }
