import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
import firebase from 'firebase/app';
import 'firebase/firestore';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { UserInterface } from '@suprsoftware/models/users';
import { LicenseKey } from '../../../lib/models/license-key.model';
import { UserDiscordConnectedEvent } from 'src/app/lib/models/events';

@Injectable({
  providedIn: 'root'
})
export class DbService {
  // might be worthwhile to cache the userref
  constructor(
    private firestore: AngularFirestore,
  ) { }

  getUserValueChanges(uid: string): Observable<UserInterface | null> {
    return this.getUserDocument(uid).valueChanges().pipe(
      switchMap((user: UserInterface | undefined) => {
        if (!!user) { return of(user); }
        return of(null);
      })
    );
  }

  getUserDocument(uid: string): AngularFirestoreDocument<UserInterface> {
    return this.firestore.doc<UserInterface>(`users/${uid}`);
  }

  updateUserDisplayName(uid: string, displayName: string): Promise<void> {
    return this.getUserDocument(uid).update({ displayName });
  }

  getUserKeysCollectionValueChanges(uid: string): Observable<LicenseKey[]> {
    return this.getUserAvailableKeysCollection(uid).valueChanges();
  }

  getUserAvailableKeysCollection(uid: string): AngularFirestoreCollection<LicenseKey> {
    return this.firestore.collection<LicenseKey>(`users/${uid}/keys`, ref => {
      return ref
        .where('expires_at', '>', new Date());
    });
  }

  unbindLicenseKeyFromMachine(key: LicenseKey): Promise<void> {
    return this.updateLicenseKeyFields(key, {
      hardware_id: '',
      hostname: '',
    });
  }

  updateLicenseKeyFields({ user_id, uuid }: LicenseKey, data: Partial<LicenseKey>): Promise<void> {
    return this.getLicenseKeyDocument(user_id, uuid).update({
      ...data
    });
  }

  getLicenseKeyDocument(userId: string, uuid: string): AngularFirestoreDocument<LicenseKey> {
    return this.firestore.doc<LicenseKey>(`users/${userId}/keys/${uuid}`);
  }

}
