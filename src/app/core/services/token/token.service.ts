import { Injectable, OnDestroy } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable, Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TokenService implements OnDestroy {

  idTokenSub: Subscription;
  accessTokenInterval!: number;

  constructor(
    private auth: AngularFireAuth,
  ) {
    this.idTokenSub = this.idToken$.subscribe({
      next: (idToken) => {
        if (!!idToken) {
          this.fetchAccessTokenHandler(idToken);
          this.setAccessTokenInterval(idToken);
        }
      },
    });
  }

  ngOnDestroy(): void {
    if (!!this.idTokenSub) { this.idTokenSub.unsubscribe(); }
  };

  get idToken$(): Observable<string | null> {
    return this.auth.idToken;
  };

  private async fetchAccessTokenHandler(idToken: string) {
    try {
      const response = await this.fetchAccessToken(idToken);
      // handle response codes 200, 400, 401, 500
      console.log(response);
    } catch(err) {
      console.log(err);
    };
  };

  private fetchAccessToken(idToken: string) {
    return fetch('https://auth.suprsoftware.io/token', {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${idToken}`
      },
      credentials: 'include'
    });
  };

  private setAccessTokenInterval(idToken: string) {
    if (this.accessTokenInterval) { clearInterval(this.accessTokenInterval); }
    this.accessTokenInterval = setInterval(this.fetchAccessTokenHandler.bind(this), 60*60*1000, idToken);
  };
}
