import { Injectable } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RouterService {

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) { 
  }

  get queryParams(): Observable<Params> {
    return this.activatedRoute.queryParams;
  };

  navigateToDashboard(): Promise<boolean> {
    return this.router.navigate(['/']);
  }

  navigateToLogin(): Promise<boolean> {
    return this.router.navigate(['/login']);
  }
}
