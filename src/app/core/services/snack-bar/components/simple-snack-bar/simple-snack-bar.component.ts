import { Component, Inject, OnInit } from '@angular/core';
import { MAT_SNACK_BAR_DATA, MatSnackBarRef } from '@angular/material/snack-bar';

@Component({
  selector: 'app-simple-snack-bar',
  templateUrl: './simple-snack-bar.component.html',
  styleUrls: ['./simple-snack-bar.component.scss']
})
export class SimpleSnackBarComponent implements OnInit {

  constructor(
    public ref: MatSnackBarRef<SimpleSnackBarComponent>,
    @Inject(MAT_SNACK_BAR_DATA) public data: string,
  ) { }

  ngOnInit(): void {
  }

  dismiss(): void {
    this.ref.dismiss();
  }
}
