import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarRef } from '@angular/material/snack-bar';
import { SimpleSnackBarComponent, LoadingSnackBarComponent } from './components';
@Injectable({
  providedIn: 'root'
})
export class SnackBarService {

  constructor(
    private snackBar: MatSnackBar,
  ) { }

  openGood(message: string): MatSnackBarRef<SimpleSnackBarComponent> {
    return this.openSimple(`✔️ ${message}`);
  }

  openBad(message: string): MatSnackBarRef<SimpleSnackBarComponent>{
    return this.openSimple(`❌ ${message}`);
  }

  openSimple(message: string): MatSnackBarRef<SimpleSnackBarComponent> {
    return this.snackBar.openFromComponent(SimpleSnackBarComponent, {
      data: message,
      verticalPosition: 'top',
      panelClass: 'snackbar',
      duration: 2500,
    });
  }

  openLoading(message?: string): MatSnackBarRef<LoadingSnackBarComponent> {
    return this.snackBar.openFromComponent(LoadingSnackBarComponent, {
      data: message || 'Loading',
      verticalPosition: 'top',
      panelClass: 'snackbar',
    });
  }
}
