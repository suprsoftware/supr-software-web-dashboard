import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import firebase from 'firebase/app';
import 'firebase/auth';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { UserInterface } from '@suprsoftware/models/users';
import { DbService } from '../db/db.service';
import { RouterService } from '../router/router.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user$: Observable<UserInterface | null>;
  isLoading$: BehaviorSubject<boolean>;

  constructor(
    private db: DbService,
    private router: RouterService,
    private auth: AngularFireAuth
  ) {
    this.isLoading$ = new BehaviorSubject<boolean>(false);
    this.user$ = this.auth.authState.pipe(
      switchMap((user: firebase.User | null) => {
        if (!!user) { return this.db.getUserValueChanges(user.uid); }
        return of(null);
      }),
      tap({
        next: (user: UserInterface | null) => {
          this.isLoading$.next(false);
          if (!user) { this.router.navigateToLogin(); }
        }
      })
    );
  }

  signOut(): Promise<void> {
    this.isLoading$.next(false);
    return this.auth.signOut();
  }

  signIn(email: string, password: string): Promise<firebase.auth.UserCredential> {
    this.isLoading$.next(true);
    return this.auth.signInWithEmailAndPassword(email, password);
  }

  createUserWithEmailAndPassword(email: string, password: string): Promise<firebase.auth.UserCredential> {
    return this.auth.createUserWithEmailAndPassword(email, password);
  }

  sendPasswordResetEmail(email: string): Promise<void> {
    return this.auth.sendPasswordResetEmail(email);
  }

}
