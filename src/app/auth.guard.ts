import { Injectable } from '@angular/core';
import { CanActivate, CanLoad, Route, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AuthService } from './core/services/auth/auth.service';
import { RouterService } from './core/services/router/router.service';
import { UserInterface } from '@suprsoftware/models/users';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanLoad {
  constructor(
    private auth: AuthService,
    private router: RouterService,
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      return this.auth.user$.pipe(
        map((user: UserInterface | null) => !!user),
        tap({
          next: (isLoggedIn: boolean) => {
            if (!isLoggedIn) {
              console.log('Unauthorized access.');
              this.router.navigateToLogin();
            }
          },
          error: () => {
            console.log('Error authenticating user.');
            this.router.navigateToLogin();
          }
        })
      );
  }
  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      return this.auth.user$.pipe(
        map((user: UserInterface | null) => !!user),
        tap({
          next: (isLoggedIn: boolean) => {
            if (!isLoggedIn) {
              console.log('Unauthorized access.');
              this.router.navigateToLogin();
            }
          },
          error: () => {
            console.log('Error authenticating user.');
            this.router.navigateToLogin();
          }
        })
      );
  }
}
