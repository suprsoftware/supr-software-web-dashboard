import firebase from 'firebase';
import 'firebase/firestore';
import { ROLE, RENEWAL_PERIOD, LicenseKeyInterface, LicenseKeySettingsInterface } from '@suprsoftware/models/license-key';

export interface LicenseKey extends LicenseKeyInterface<firebase.firestore.Timestamp> {
  activated_at: firebase.firestore.Timestamp;
  created_at: firebase.firestore.Timestamp;
  expires_at: firebase.firestore.Timestamp;
  hardware_id: string;
  hostname: string;
  lastlogin_at: firebase.firestore.Timestamp;
  renewal_period: RENEWAL_PERIOD;
  role: ROLE;
  settings: LicenseKeySettingsInterface;
  user_id: string;
  uuid: string;
}
