import { UserDiscordInterface } from '@suprsoftware/models/users';
export interface UserCardUpdateUsernameEvent {
  uid: string;
  displayName: string;
}

export interface UserDiscordConnectedEvent {
  uid: string;
  discord: UserDiscordInterface;
}