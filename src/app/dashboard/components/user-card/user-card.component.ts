import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { UserInterface } from '@suprsoftware/models/users';
import { UserCardUpdateUsernameEvent } from 'src/app/lib/models/events';
import { RouterService } from 'src/app/core/services/router/router.service';
import { Subscription } from 'rxjs';
import { faDiscord } from '@fortawesome/free-brands-svg-icons';
import { SnackBarService } from 'src/app/core/services/snack-bar/snack-bar.service';
@Component({
  selector: 'app-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.scss']
})

export class UserCardComponent implements OnInit {
  usernameControl: FormControl;
  queryParamsSub: Subscription | null;

  @Input() user!: UserInterface;
  @Output() logOutEvent: EventEmitter<void>;
  @Output() updateUsernameEvent: EventEmitter<UserCardUpdateUsernameEvent>;
  @Output() resetPasswordEvent: EventEmitter<string>;
  @Output() discordDisconnectEvent: EventEmitter<string>;
  constructor(
    private router: RouterService,
    private snackBar: SnackBarService,
  ) {
    this.logOutEvent = new EventEmitter();
    this.updateUsernameEvent = new EventEmitter();
    this.resetPasswordEvent = new EventEmitter();
    this.discordDisconnectEvent = new EventEmitter();
    this.usernameControl = new FormControl('');
    this.queryParamsSub = null;
  }

  get discordIcon(): any {
    return faDiscord;
  }

  get hasDiscord(): boolean {
    const discord = this.user.discord;
    return !!discord && 
      !!discord?.id && 
      !!discord?.username && 
      !!discord?.discriminator;
  }

  ngOnInit(): void {
    this.queryParamsSub = this.router.queryParams.subscribe({
      next: ({discordToken}) => {
        if (!!discordToken) { this.bindUserDiscord(discordToken); }
      },
    });
  }

  ngOnDestroy(): void {
    if (!!this.queryParamsSub) { this.queryParamsSub.unsubscribe(); }
  }

  logOut(): void {
    this.logOutEvent.emit();
  }

  usernameChange(): void {
    this.updateUsernameEvent.emit({
      uid: this.user.uid,
      displayName: this.usernameControl.value,
    });
  }

  resetPassword(): void {
    this.resetPasswordEvent.emit(this.user.email);
  }

  goToDiscord(): void {
    window.open("https://discord.suprsoftware.io/login", "_self");
  }

  bindUserDiscord(discordToken: string) {
    fetch('https://discord.suprsoftware.io/bind', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json' 
      },
      body: JSON.stringify({
        discordToken,
      }),
      credentials: 'include'
    })
      .then(res => {
        if (res.status == 200) {
          this.snackBar.openGood('Bound Discord');
        } else {
          this.snackBar.openBad('Failed to bind Discord');
        };
      })
      .catch(console.error);
  };

  discordDisconnectHanlder() {
    fetch('https://discord.suprsoftware.io/unbind',{
      method: 'GET',
      credentials: 'include'
    })
      .then(res => {
        if (res.status == 200) {
          this.snackBar.openGood('Unbound Discord');
        } else {
          this.snackBar.openBad('Failed to unbind Discord');
        }
      })
      .catch(console.error)
  };
}
