import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UserDiscordInterface } from '@suprsoftware/models/users';

@Component({
  selector: 'app-user-discord-card',
  templateUrl: './user-discord-card.component.html',
  styleUrls: ['./user-discord-card.component.scss']
})
export class UserDiscordCardComponent implements OnInit {
  svgIcon: string;

  @Input() discord!: UserDiscordInterface;
  @Input() discordIcon!: any;
  @Output() discordDisconnectEvent: EventEmitter<void>;
  constructor() { 
    this.svgIcon = "link";
    this.discordDisconnectEvent = new EventEmitter();
  }

  get discordAvatarURL() {
    return `https://cdn.discordapp.com/avatars/${this.discord.id}/${this.discord.avatar}.png`;
  };

  ngOnInit(): void {
  }

  mouseEnter() {
    this.svgIcon = "link_off";
  }

  mouseLeave() {
    this.svgIcon = "link";
  }

  disconnectDiscord() {
    this.discordDisconnectEvent.emit();
  };
  
}
