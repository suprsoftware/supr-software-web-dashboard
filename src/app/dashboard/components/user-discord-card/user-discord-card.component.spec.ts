import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserDiscordCardComponent } from './user-discord-card.component';

describe('UserDiscordCardComponent', () => {
  let component: UserDiscordCardComponent;
  let fixture: ComponentFixture<UserDiscordCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserDiscordCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDiscordCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
