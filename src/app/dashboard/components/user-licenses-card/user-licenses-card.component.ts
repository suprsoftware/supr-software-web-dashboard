import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { DbService } from 'src/app/core/services/db/db.service';
import { LicenseKey } from 'src/app/lib/models/license-key.model';
import { UserInterface } from '@suprsoftware/models/users';

@Component({
  selector: 'app-user-licenses-card',
  templateUrl: './user-licenses-card.component.html',
  styleUrls: ['./user-licenses-card.component.scss']
})
export class UserLicensesCardComponent implements OnInit {
  keys$: Observable<LicenseKey[]>;
  keySelectionControl: FormControl;

  @Input() user!: UserInterface;
  constructor(
    private db: DbService
  ) {
    this.keys$ = of([]);
    this.keySelectionControl = new FormControl();
  }

  get keySelectionCount(): number {
    return !!this.keySelectionControl.value ? this.keySelectionControl.value.length : 0;
  }

  ngOnInit(): void {
    this.keys$ = this.db.getUserKeysCollectionValueChanges(this.user.uid);
  }

  renew(): void {}

  reset(): void {
    const keys: LicenseKey[] = this.keySelectionControl.value;
    keys.forEach(this.db.unbindLicenseKeyFromMachine, this.db);
  }

}
