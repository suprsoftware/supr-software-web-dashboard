import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserLicensesCardComponent } from './user-licenses-card.component';

describe('UserLicensesCardComponent', () => {
  let component: UserLicensesCardComponent;
  let fixture: ComponentFixture<UserLicensesCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserLicensesCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserLicensesCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
