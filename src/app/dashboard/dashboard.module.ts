import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { DashboardComponent } from './dashboard.component';
import { CoreModule } from '../core/core.module';
import { UserCardComponent } from './components/user-card/user-card.component';
import { UserLicensesCardComponent } from './components/user-licenses-card/user-licenses-card.component';
import { SharedModule } from '../shared/shared.module';
import { UserDiscordCardComponent } from './components/user-discord-card/user-discord-card.component';

@NgModule({
  declarations: [DashboardComponent, UserCardComponent, UserLicensesCardComponent, UserDiscordCardComponent],
  imports: [
    CoreModule,
    SharedModule,
    MatCardModule,
    MatIconModule,
    MatListModule,
  ]
})
export class DashboardModule { }
