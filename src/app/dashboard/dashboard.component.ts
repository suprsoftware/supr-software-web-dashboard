import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../core/services/auth/auth.service';
import { DbService } from '../core/services/db/db.service';
import { UserInterface } from '@suprsoftware/models/users';
import { UserCardUpdateUsernameEvent } from '../lib/models/events';
import { faApple, faWindows } from '@fortawesome/free-brands-svg-icons';
import { TokenService } from '../core/services/token/token.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  user$: Observable<UserInterface | null>;
  isLoading$: Observable<boolean>;

  constructor(
    private auth: AuthService,
    private db: DbService,
    private token: TokenService,
  ) {
    this.user$ = this.auth.user$;
    this.isLoading$ = this.auth.isLoading$.pipe();
  }

  get appleIcon(): any {
    return faApple;
  }

  get windowsIcon(): any {
    return faWindows;
  }

  ngOnInit(): void {
  }

  logOut(): void {
    this.auth.signOut();
  }

  updateUsername({uid, displayName}: UserCardUpdateUsernameEvent): void {
    this.db.updateUserDisplayName(uid, displayName);
  }

  sendResetPasswordLink($event: string): void {
    this.auth.sendPasswordResetEmail($event);
  }

}
