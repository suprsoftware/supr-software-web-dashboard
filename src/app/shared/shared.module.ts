import { NgModule } from '@angular/core';
import { LicenseKeyPipe } from './pipes';
@NgModule({
  declarations: [
    LicenseKeyPipe
  ],
  imports: [
  ],
  exports: [
    LicenseKeyPipe
  ]
})
export class SharedModule { }
