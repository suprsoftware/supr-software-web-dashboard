import { LicenseKeyPipe } from './license-key.pipe';

describe('LicenseKeyPipe', () => {
  const pipe = new LicenseKeyPipe();

  it('pipes "abdc-abdc-abcd" to "XXXX-XXXX-abcd"', () => {
    expect(pipe.transform('abdc-abdc-abcd')).toBe('XXXX-XXXX-abcd');
  });

  it('pipes "" to ""', () => {
    expect(pipe.transform('')).toBe('');
  });
});