import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'licenseKey'})
export class LicenseKeyPipe implements PipeTransform {
  transform(value: string): string {
    const keyFrags = value.split('-');
    const keyFragLength = keyFrags.length;
    return keyFrags
      .map((frag: string, i: number) => {
        if (i === keyFragLength - 1) { return frag; }
        return frag.split('').map(c => 'x').join('');
      })
      .join('-');
  };
}