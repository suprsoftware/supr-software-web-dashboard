export const AppConfig = {
  production: true,
  firebaseConfig: {
    apiKey: 'AIzaSyD5UlrJEer7xBL7ls6f0puCvrIbl45gtZk',
    authDomain: 'supr-software-prod.firebaseapp.com',
    databaseURL: 'https://supr-software-prod.firebaseio.com',
    projectId: 'supr-software-prod',
    storageBucket: 'supr-software-prod.appspot.com',
    messagingSenderId: '1043934384474',
    appId: '1:1043934384474:web:c118881f333f51bcee6363',
    measurementId: 'G-0R3NEN60QP'
  }
};
