// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const AppConfig = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyAjd4819ZXFWfyAztkb6K8ehqSO8eeuSu8',
    authDomain: 'supr-software-dev.firebaseapp.com',
    databaseURL: 'https://supr-software-dev.firebaseio.com',
    projectId: 'supr-software-dev',
    storageBucket: 'supr-software-dev.appspot.com',
    messagingSenderId: '35622939465',
    appId: '1:35622939465:web:aa719e8ecce0807542c898',
    measurementId: 'G-88SS65Q9VH'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
